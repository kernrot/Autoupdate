namespace GitLabUpdater.Json
{
    public class ArtifactFile
    {
        public string filename { get; set; }
        public int size { get; set; }
    }
}